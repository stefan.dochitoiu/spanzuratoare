var secretWord = '', guessedLetters = '', failedLetters = '', failedWords = new Array(), nextTrial = 1;

function requestForUserInput(message) {
   let returned = prompt(message);
   while (!returned) {
      returned = prompt('You have to enter some letter (s) and hit "OK"');
   }
   return returned;
}

function guessLetters() {
   let newGame = function() {
      document.getElementById("guessedLetters").innerHTML += 'Now hit the "SECRET WORD" button if you like to play again';
      secretWord = guessedLetters = failedLetters = '';
      failedWords = new Array();
      nextTrial = 1;
   }
   let isLetter = function(character) {
      const a = "a".charCodeAt(0), z = "z".charCodeAt(0), A = "A".charCodeAt(0), Z = "Z".charCodeAt(0), char = character.charCodeAt(0);
      if ((a <= char && char <= z) || (A <= char && char <= Z)) {
         return true;
      }
      return false;
   }
   let meetsGivenConditions = function() {
      let showGuessedLetters = document.getElementById("showGuessedLetters").innerHTML.trim(), returned = true;
      if (letters.length !== secretWord.length) {
         returned = false;
      }
      for (let i = 0; i < letters.length; ++i) {
         let letter = letters.charAt(i), guessedletter = showGuessedLetters.charAt(2 * i + 1);
         if (!isLetter(letter)) {
            message = 'The characters you entered are not all valid letters';
            returned = false;
         } else if (guessedletter !== letter && guessedletter !== "_") {
            returned = false;
         }
      }
      return returned;
   }
   let reaction = function(message) {
      if (message === "fail" && nextTrial < 15) {
         document.getElementById("trialsLeft").children[nextTrial++].innerHTML += 'FAILED';
         document.getElementById("trialsLeft").children[0].innerHTML = 'TRIALS LEFT (' + (15 - nextTrial) + ' FAILED TRIALS ALLOWED YET)';
      } else if (message === "fail") {
         document.getElementById("guessedLetters").innerHTML = 'You lose<br>The secret \
         word was: "' + secretWord + '"<br>';
         newGame();
      } else if (message === "won" && (guessedLetters.length === secretWord.length || letters === secretWord)) {
         document.getElementById("guessedLetters").innerHTML = 'You won!!!<br>';
         newGame();
      }
   }
   let updateGuesses = function () {
      if (letters === secretWord) {
         return true;
      } else if (letters.length > 1) {
         failedWords.push(letters);
         return false;
      } else if (singleLetter) {
         let returned = false, remained = secretWord, letterIndex = remained.indexOf(letters), foundIndex = letterIndex;
         while (foundIndex > -1) {
            returned = true;
            guessedLetters += letters;
            let showGuessedLetters = document.getElementById("showGuessedLetters").innerHTML.trim();
            document.getElementById("showGuessedLetters").innerHTML = showGuessedLetters.substring(0, 2 * letterIndex + 1) + letters + showGuessedLetters.substring(2 * letterIndex + 2);
            remained = remained.substring(foundIndex + 1);
            foundIndex = remained.indexOf(letters);
            letterIndex += foundIndex + 1;
        }
        if (!returned) {
           failedLetters += letters;
        }
        return returned;
      }
  }

  let letters = requestForUserInput("Enter the guessed letter or word").toUpperCase(), singleLetter = letters.length === 1;
  let message = 'The word you entered does not meet the given/reached already knowm conditions';
  if (singleLetter && !isLetter(letters)) {
     alert('The character you entered is not a valid letter');
  } else if (!singleLetter && !meetsGivenConditions()) {
     alert(message);
  } else if (singleLetter && guessedLetters.indexOf(letters) > -1) {
     alert('This letter was yet guessed');
  } else if (singleLetter && failedLetters.indexOf(letters) > -1) {
     alert('This letter was yet in a failed trial');
  } else if (!singleLetter && failedWords.includes(letters)) {
     alert('This word was yet in a failed trial');
  } else if ((updateGuesses() && singleLetter) || letters === secretWord) {
     reaction("won");
  } else {
     reaction("fail");
  }
}

function gameSpace() {
   secretWord = requestForUserInput("Enter the secret word:").toUpperCase();
   let placeHolders = '|';
   for (const letter of secretWord) {
      placeHolders += '_|';
   }
   document.getElementById("guessedLetters").innerHTML = '<p>Guessed letters (from \
   ' + secretWord.length + ' letters): <label id = "showGuessedLetters">\
   ' + placeHolders + '</label>&nbsp<button onClick="guessLetters();">GUESS !\
   </button></p>';
   let htmlCode = '<p>TRIALS LEFT (' + (15 - nextTrial) + ' FAILED TRIALS ALLOWED YET)</p>';
   htmlCode += '<p id="head">HEAD &#8594 First trial:&nbsp </p>';
   htmlCode += '<p id="trunk">TRUNK &#8594 Second trial:&nbsp </p>';
   htmlCode += '<p id="leftLeg">LEFT LEG &#8594 Third trial:&nbsp </p>';
   htmlCode += '<p id="rightLeg">RIGHT LEG &#8594 Fourth trial:&nbsp </p>';
   htmlCode += '<p id="leftArm">LEFT ARM &#8594 Fifth trial:&nbsp </p>';
   htmlCode += '<p id="rightArm">RIGHT ARM &#8594 Sixth trial:&nbsp </p>';
   htmlCode += '<p id="leftFoot">LEFT FOOT &#8594 Seventh trial:&nbsp </p>';
   htmlCode += '<p id="rightFoot">RIGHT FOOT &#8594 Eighth trial:&nbsp </p>';
   htmlCode += '<p id="leftHand">LEFT HAND &#8594 Ninth trial:&nbsp </p>';
   htmlCode += '<p id="rightHand">RIGHT HAND &#8594 Tenth trial:&nbsp </p>';
   htmlCode += '<p id="leftEye">LEFT EYE &#8594 Eleventh trial:&nbsp </p>';
   htmlCode += '<p id="rigthEye">RIGHT EYE &#8594 Twelfth trial:&nbsp </p>';
   htmlCode += '<p id="nose">NOSE &#8594 Thirteenth trial:&nbsp </p>';
   htmlCode += '<p id="mouth">MOUTH &#8594 Fourteenth trial:&nbsp </p>';
   document.getElementById("trialsLeft").innerHTML = htmlCode;
}
